package com.currency.converter.client.rest;

import com.currency.converter.client.services.ConverterService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ConvertController.class)
class ConvertControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ConverterService service;

    @Test
    void convert() throws Exception {
        String currencyRate = "1.2";
        given(service.getCurrencyRate("EUR", "USD"))
                .willReturn(currencyRate);

        mockMvc.perform(get("/api/v1/convert?base_currency=EUR&currency=USD&amount=100"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("value").value("120"));
    }

    @Test
    void value() {
    }
}