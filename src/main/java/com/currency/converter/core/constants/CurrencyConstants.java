package com.currency.converter.core.constants;

public class CurrencyConstants {
    public static final String BASE_CURRENCY = "base_currency";
    public static final String API_KEY = "apiKey";
}
