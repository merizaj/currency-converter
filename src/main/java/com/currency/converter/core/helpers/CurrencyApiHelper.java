package com.currency.converter.core.helpers;

import com.currency.converter.client.exceptions.InvalidInputException;
import com.currency.converter.config.Config;
import com.currency.converter.core.constants.CurrencyConstants;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.util.*;

@Slf4j
public class CurrencyApiHelper {

    private static final String API_CURRENCIES_OBJECT = "data";

    public static String buildUrl(Config config, Map<String, String> parameters) {
        StringBuilder urlBuilder = new StringBuilder().append(config.getUrl()).append("?")
                .append(CurrencyConstants.API_KEY + "=")
                .append(config.getApiKey());

        for (Map.Entry<String, String> parameter : parameters.entrySet()) {
            urlBuilder.append("&").append(parameter.getKey()).append("=").append(parameter.getValue());
        }
        return urlBuilder.toString();
    }

    public static String getRateFromJson(String currencies, String currency) throws Exception {
        String rate;
        try {
            JSONObject jObject = new JSONObject(currencies); // json
            JSONObject data = jObject.getJSONObject(API_CURRENCIES_OBJECT);

            Iterator keys = data.keys();
            String currenciesNode = (String) keys.next();

            JSONObject currenciesList;
            if (currenciesNode != null) {
                currenciesList = data.getJSONObject(currenciesNode);
            } else {
                throw new Exception("API internal error");
            }
            try {
                rate = currenciesList.getString(currency);
            } catch (Exception e) {
                throw new Exception("Currency \"" + currency + "\" doesn't exist in our API service!");
            }
           log.debug("RATE" + rate);
        } catch (Exception e) {
            throw new Exception("API internal error");
        }
        return rate;
    }

    public static void validateParams(String baseCurrency, String currency, String amount) throws InvalidInputException {
        if (baseCurrency.length() != 3) {
            throw new InvalidInputException("Base currency code is not valid!");
        }
        if (currency.length() != 3) {
            throw new InvalidInputException("Currency currency code is not valid!");
        }
        try {
            if (amount.isEmpty() || Double.isNaN(Double.parseDouble(amount))) {
                throw new InvalidInputException("Amount is not valid decimal value!");
            }
        }catch (NumberFormatException e){
            throw new InvalidInputException("Amount is not valid decimal value!");
        }

    }
}
