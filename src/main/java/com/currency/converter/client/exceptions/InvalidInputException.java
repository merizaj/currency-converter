package com.currency.converter.client.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidInputException extends RuntimeException {

    private static final long serialVersionUID = -5027121014723838738L;

    public InvalidInputException(String message){
        super(message);
    }
}
