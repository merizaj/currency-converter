package com.currency.converter.client.rest;

import com.currency.converter.client.responses.APIResponse;
import com.currency.converter.client.responses.ErrorResponse;
import com.currency.converter.client.services.ConverterService;
import com.currency.converter.core.helpers.CurrencyApiHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
@Api(value = "Currency converter")
public class ConvertController {

    @Autowired
    ConverterService converterService;

    @ApiOperation(value = "Get the converted amount of money in the desired currency")
    @GetMapping("/convert")
    public ResponseEntity<Response> convert(
            @ApiParam(value = "Currency Code for the given amount of money", required = true) @RequestParam(value = "base_currency") String baseCurrency,
            @ApiParam(value = "Currency Code of the requested converted amount of money", required = true) @RequestParam(value = "currency") String currency,
            @ApiParam(value = "The amount of money to be converted", required = true) @RequestParam(value = "amount") String amount
    ){
        String value;
        CurrencyApiHelper.validateParams(baseCurrency, currency, amount);
        APIResponse response = new APIResponse("");
        try{
            Executor executor = Executors.newCachedThreadPool();
            CompletableFuture<String> currencyRates = CompletableFuture.supplyAsync(() -> converterService.getCurrencyRate(baseCurrency, currency), executor);
            value = converterService.convert(currencyRates.get(), amount);
            response.setValue(value);
        }catch (Exception e){
            log.debug("ERROR:",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e.getMessage(), List.of("")));
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

}
