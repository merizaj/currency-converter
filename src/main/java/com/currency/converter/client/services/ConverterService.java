package com.currency.converter.client.services;

import com.currency.converter.client.requests.CurrencyAPIRequest;
import com.currency.converter.client.requests.CurrencyAPIResponse;
import com.currency.converter.core.constants.CurrencyConstants;
import com.currency.converter.core.helpers.CurrencyApiHelper;
import com.currency.converter.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class ConverterService {
    @Autowired
    Config config;

    public String getCurrencyRate(String base_currency, String currency) {
        try {

            HttpClient client = HttpClient.newHttpClient();
            Map<String, String> parameters = new HashMap<>();
            parameters.put(CurrencyConstants.BASE_CURRENCY, base_currency);
            String url = CurrencyApiHelper.buildUrl(config, parameters);

            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .timeout(Duration.ofMinutes(1))
                    .header("Content-Type", "application/json")
                    .GET()
                    .build();
            HttpResponse<String> reponse = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (reponse.statusCode() == HttpURLConnection.HTTP_OK) {
                return CurrencyApiHelper.getRateFromJson(reponse.body(), currency);
            } else {
                throw new Exception("Error in API Call");
            }
        } catch (Exception e) {
            log.debug("ConverterService: " + e.getMessage());
        }
        return "";
    }

    public String convert(String rate, String amount) throws Exception {
        double value;
        value = Double.parseDouble(amount) * Double.parseDouble(rate);
        return String.valueOf(value);
    }
}
