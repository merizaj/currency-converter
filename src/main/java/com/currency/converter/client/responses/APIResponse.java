package com.currency.converter.client.responses;

import io.swagger.models.Response;


public class APIResponse extends Response {


    private String value;

    public APIResponse(String value) {
        this.value = value;

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
