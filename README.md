# Currency-Converter



## Getting started
This Currency converter API is used to get the converted value for a given amount on a specific currency to another one.
- Run the project and visit: http://localhost:8081/api/v1/

An example endpoint call to this API:

- http://localhost:8081/api/v1/convert?base_currency=EUR&currency=USD&amount=100.00


API documentation:
http://localhost:8081/swagger-ui.html
## License
Apache License
Version 2.0, January 2004

http://www.apache.org/licenses/
